﻿class MainClass
{
    public static void Main(string[] args)
    {
        bool cycle = true;
        while (cycle)
        {
            Console.WriteLine("1.Инициализация магазина\n2.Удаление товара\n3.Показать все товары\n4.Отсортировать товары по цене\n5.Вегетарианские товары\n6.Съедобное/Несъедобное\n7.Выход");
            int key = Convert.ToInt32(Console.ReadLine());
            switch (key)
            {
                case 1:
                    Console.Clear();
                    Shop.InitializeShop();
                    break;
                case 2:
                    Console.Clear();
                    Console.WriteLine("Введите имя товара который нужно удалить");
                    string productToRemove = Console.ReadLine();
                    Shop.Remove(productToRemove);
                    break;
                case 3:
                    Console.Clear();
                    Shop.ShowAll();
                    Console.WriteLine("\nНажмите любую кнопку чтобы продолжить");
                    Console.ReadLine();
                    Console.Clear();
                    break;
                case 4:
                    Console.Clear();
                    Shop.ShowAllSort();
                    Shop.ShowAll();
                    Console.WriteLine("\nНажмите любую кнопку чтобы продолжить");
                    Console.ReadLine();
                    Console.Clear();
                    break;
                case 5:
                    Console.Clear();
                    Shop.GetVegan();
                    Console.WriteLine("\nНажмите любую кнопку чтобы продолжить");
                    Console.ReadLine();
                    Console.Clear();
                    break;
                case 6:
                    Shop.GetSortedProducts();
                    break;
                case 7:
                    cycle = false;
                    break;
            }
        }
    }
}

class Shop
{
    public static List<Product> productList = new List<Product>();

    public static void InitializeShop()
    {
        Add(new Fruit("Яблоко", 1, "Беларусь"));
        Add(new Fruit("Апельсин", 3, "Турция"));
        Add(new Vegetables("Картошка", 2, "Беларусь"));
        Add(new Vegetables("Помидор", 1, "Украина"));
        Add(new Vegetables("Морковка", 3, "Россия"));
        Add(new NonVegetarian("Мясо", 10, "Беларусь"));
        Add(new NonVegetarian("Рыба", 8, "Норвегия"));
        Add(new Furniture("Стол", 350, "Швеция"));
        Add(new Furniture("Стул", 100, "Швеция"));
        Add(new Furniture("Шкаф", 150, "Швеция"));
        Add(new Chancery("Карандаш", 3, "Польша"));
        Add(new Chancery("Клей", 4, "Китай"));
    }

    public static void Add(Product productToAdd)
    {
        productList.Add(productToAdd);
    }
    public static void Remove(string name)
    {
        foreach (Product product in productList)
        {
            if (product.Name == name)
            {
                Console.Clear();
                productList.Remove(product);
                Console.WriteLine($"Товар {name} удален из списка товаров.\nНажмите любую кнопку чтобы продолжить");
                Console.ReadLine();
                Console.Clear();
                return;
            }
        }
    }
    public static void ShowAll()
    {
        for (int i = 0; i < productList.Count; i++)
        {
            Console.WriteLine(productList[i].ToString());
        }
        Console.WriteLine($"Всего продуктов:{productList.Count} шт.");
    }
    public static void ShowAllSort()
    {
        productList.Sort((left, right) => left.Price.CompareTo(right.Price));
    }
    public static void GetVegan()
    {
        List<Product> productListSort = new List<Product>();
        for (int i = 0; i < productList.Count; i++)
        {
            if (productList[i] is Vegetarian)
            {
                productListSort.Add(productList[i]);
            }
        }

        for (int i = 0; i < productListSort.Count; i++)
        {
            Console.WriteLine(productListSort[i].ToString());
        }

    }
    public static void GetSortedProducts()
    {
        List<Product> productListEdible = new List<Product>();
        List<Product> productListUnedible = new List<Product>();
        Console.WriteLine("1.Показать съедобные продукты\n2.Показать несъедобные продукты");
        int key = Convert.ToInt32(Console.ReadLine());
        switch (key)
        {
            case 1:
                Console.Clear();
                for (int i = 0; i < productList.Count; i++)
                {
                    if (productList[i].IsEdible)
                    {
                        productListEdible.Add(productList[i]);
                    }
                }
                for (int i = 0; i < productListEdible.Count; i++)
                {
                    Console.WriteLine(productListEdible[i].ToString());
                }
                Console.WriteLine($"Всего продуктов:{productListEdible.Count} шт.");
                Console.WriteLine("\nНажмите любую кнопку чтобы продолжить");
                Console.ReadLine();
                Console.Clear();
                break;
            case 2:
                Console.Clear();
                for (int i = 0; i < productList.Count; i++)
                {
                    if (!productList[i].IsEdible)
                    {
                        productListUnedible.Add(productList[i]);
                    }
                }
                for (int i = 0; i < productListUnedible.Count; i++)
                {
                    Console.WriteLine(productListUnedible[i].ToString());
                }
                Console.WriteLine($"Всего продуктов:{productListUnedible.Count} шт.");
                Console.WriteLine("\nНажмите любую кнопку чтобы продолжить");
                Console.ReadLine();
                Console.Clear();
                break;
        }
    }

    public abstract class Product
    {
        protected string _name;
        protected int _price;
        protected string _country;
        protected bool _isEdible;
        public bool IsEdible => _isEdible;
        public string Name => _name;
        public int Price => _price;
        public string Country => _country;
        public Product(string name, int price, string country)
        {
            _name = name;
            _price = price;
            _country = country;
        }
        public abstract string ToString();
    }

    interface IEdible
    {
        void SetEdible();
    }

    class Vegetarian : Product, IEdible
    {
        public Vegetarian(string name, int price, string country) : base(name, price, country)
        {
            SetEdible();
        }
        public override string ToString()
        {
            return $"Название: {Name}\nЦена: {Price}\nСтрана: {Country}";
        }
        public void SetEdible()
        {
            _isEdible = true;
        }
    }

    class NonVegetarian : Product, IEdible
    {
        public NonVegetarian(string name, int price, string country) : base(name, price, country)
        {
            SetEdible();
        }
        public override string ToString()
        {
            return $"Название: {Name}\nЦена: {Price}\nСтрана: {Country}";
        }
        public void SetEdible()
        {
            _isEdible = true;
        }
    }

    class Chancery : Product
    {
        public Chancery(string name, int price, string country) : base(name, price, country)
        {

        }
        public override string ToString()
        {
            return $"Название: {Name}\nЦена: {Price}\nСтрана: {Country}";
        }
    }

    class Furniture : Product
    {
        public Furniture(string name, int price, string country) : base(name, price, country)
        {

        }
        public override string ToString()
        {
            return $"Название: {Name}\nЦена: {Price}\nСтрана: {Country}";
        }
    }

    class Fruit : Vegetarian
    {
        public Fruit(string name, int price, string country) : base(name, price, country)
        {

        }
    }
    class Vegetables : Vegetarian
    {
        public Vegetables(string name, int price, string country) : base(name, price, country)
        {

        }
    }
}

